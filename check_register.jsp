<title>Authentication Page</title>
<%@ include file="connect.jsp" %>
<%@ page import="java.util.Date" %>
<%
	String name=request.getParameter("name");
	String uname=request.getParameter("uname");
	String password=request.getParameter("password");
	String dob=request.getParameter("dob");
	String ph_no=request.getParameter("ph_no");
	String email=request.getParameter("email");
	
    try {
		PreparedStatement ps=connection.prepareStatement("insert into users(`name`, `username`, `password`, `date_of_birth`, `phone_no`, `email`) values(?,?,?,?,?,?)");
		ps.setString(1,name);
		ps.setString(2,uname);
		ps.setString(3,password);	
		ps.setString(4,dob);
		ps.setString(5,ph_no);
		ps.setString(6,email);

		int x=ps.executeUpdate();
		if(x>0) {
			out.print("Registered Successfully");
			response.sendRedirect("login.jsp");
		}
	} catch(Exception e) {
		out.print(e);
	}
%>