<title>Authentication Page</title>
<%@ include file="connect.jsp" %>
<%@ page import="java.util.Date" %>
<%
	String title=request.getParameter("title");
	String description=request.getParameter("description");
	String post_id=request.getParameter("post_id");
	
    try {
		PreparedStatement ps=connection.prepareStatement("update posts set post_title=?, post_description=?, date_updated=NOW() where post_id=?");
		ps.setString(1,title);
		ps.setString(2,description);
		ps.setString(3,post_id);

		int x=ps.executeUpdate();
		if(x>0) {
			out.print("Post updated Successfully");
			response.sendRedirect("view_post.jsp?post_id="+post_id);
		}
	} catch(Exception e) {
		out.print(e);
	}
%>