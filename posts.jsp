<!DOCTYPE html>
<html>
<head>
	<title>INT Assignment</title>
</head>
<body>
	<div class="container">
		<%@ include file="connect.jsp" %>
		<%@ include file="navbar.jsp" %>
		<%
			String s2="",s3="",s4="",s5="",s6="",s7="";
			int i=0;
			try
			{
				String query="select * from posts"; 
				Statement st=connection.createStatement();
				ResultSet rs=st.executeQuery(query);			
				%>
				<br>
				<div class="col-md-12"  style="width:1000px; margin:0 auto;">
					<div class="card">
						<div class="card-header  bg-info text-white text-center">
							Posts
							<a href="post_create.jsp" class="btn btn-success float-right">Create New Post</a>
						</div>
						<div class="card-body">
							<%
								while ( rs.next() )
								{
									i=rs.getInt(1);
									s2=rs.getString(2);
									s3=rs.getString(3);
									s4=rs.getString(4);
									s5=rs.getString(5);
									s6=rs.getString(6);
									s7=rs.getString(7);
									%>
									<a href="view_post.jsp?post_id=<%=i%>"><%=s2%>  -  created by <%=s7%></a><br>
									<%
								}
							%>
						</div>
					</div>
				</div>

				<%
			}
			catch(Exception e)
			{
				out.println(e);
			}
		%>
		<br>
	</div>
</body>
</html>
