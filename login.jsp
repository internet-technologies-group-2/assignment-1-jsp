<!DOCTYPE html>
<html>
<head>
	<title>INT Assignment</title>
</head>
<body>
	<div class="container">
		<%@ include file="navbar.jsp" %>
		<br>

		<div class="col-md-6"  style="width:1000px; margin:0 auto;">
			<div class="card">
				<div class="card-header  bg-info text-white text-center">
					Login
				</div>
				<div class="card-body">
					<form class="form-group" action="check_login.jsp" method="POST">
						<b>Username: </b><input type="text" name="uname" required class="form-control col-md-6">
						<b>Password: </b><input type="password" name="password" required class="form-control col-md-6">
						<br>
						<input type="submit" name="subBtn" value="Login" class="btn btn-success">
						<input type="reset" name="resetBtn" value="Reset" class="btn btn-danger">
					</form>
					<br>
					<a href="signup_page.jsp">Not a User? Click here to Register</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
