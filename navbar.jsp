<link rel="stylesheet" href="bootstrap.css">
<style type="text/css">
	body {
		background-color: lavender;
	}

	.alert-fixed {
		position:fixed; 
		top: 0px; 
		left: 0px; 
		width: 100%;
		z-index:9999; 
		border-radius:0px
	}
</style>
<%
    String name=(String)session.getAttribute("username");
    
    boolean display;
    String home_link;
    String current_user;

    if (name == null) {
        display = false;
        home_link = "index.jsp";
        current_user = "Guest";
    } else {
        display = true;
        home_link = "home.jsp";
        current_user = name;
    }
%> 

<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
    <a class="navbar-brand" href="<%  out.print(home_link);  %>">Team 2</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div id="navbarNavDropdown" class="navbar-collapse collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<%  out.print(home_link);  %>">Home</a>
            </li>
            <%
                if (display == true) 
                {
                	out.print("<li class='nav-item'><a class='nav-link' href='posts.jsp'>Posts</a></li>");
                }
            %>
            <li class="nav-item">
                <a class="nav-link" href="aboutus.jsp">About Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact_us.jsp">Contact Us</a>
            </li>

        </ul>
        <ul class="navbar-nav">
            <li class="nav-item active">
                <div class="nav-link">Welcome <%  out.print(current_user);  %> </div>
            </li>
            <%
                if (display) {
                    out.print("<li class='nav-item active'><a class='nav-link' href='logout.jsp' id='logoutBtn' onclick='return confirm(\'Are you Sure to Logout ?\');'>Logout</a></li>");
                } else {
                    out.print("<li class='nav-item active'><a class='nav-link' href='login.jsp'>Login</a></li><li class='nav-item active'><a class='nav-link' href='signup_page.jsp'>Register</a></li>");
	            }
            %>
        </ul>
    </div>
</nav>