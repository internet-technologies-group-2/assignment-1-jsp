<!DOCTYPE html>
<html>
<head>
	<title>INT Assignment</title>
</head>
<body>
	<div class="container">
	<%@ include file="navbar.jsp" %>
		<br>
		<div class="col-md-6"  style="width:1000px; margin:0 auto;">
			<div class="card">
				<div class="card-header  bg-info text-white text-center">
					Create Post
				</div>
				<div class="card-body">
					<form class="form-group" action="check_post_create.jsp" method="POST">
						<b>Title: </b><input type="text" name="title" required class="form-control col-md-12">
						<b>Description: </b>
						<textarea rows="4" cols="50"  name="description" required class="form-control col-md-12"></textarea>	
						<br>
						<input type="submit" name="subBtn" value="Submit" class="btn btn-success">
						<a href="posts.jsp" class="btn btn-danger float-right">Cancel</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
