<!DOCTYPE html>
<html>
<head>
	<title>INT Assignment</title>
</head>
<body>
	<div class="container">
		<%@ include file="connect.jsp" %>
		<%@ include file="navbar.jsp" %>
		<%
			String s2="",s3="",s4="",s5="",s6="",s7="";
			int i=0;
			try
			{
				String post_id=request.getParameter("post_id");
				String query="select * from posts where post_id = "+post_id;
				Statement st=connection.createStatement();
				ResultSet rs=st.executeQuery(query);			
				%>
				<br>
				<div class="col-md-12"  style="width:1000px; margin:0 auto;">
					<div class="card">
						<div class="card-header bg-info text-white text-center">
							View Post
						</div>
						<div class="card-body">
							<%
								while ( rs.next() )
								{
									i=rs.getInt(1);
									s2=rs.getString(2);
									s3=rs.getString(3);
									s4=rs.getString(4);
									s5=rs.getString(5);
									s6=rs.getString(6);
									s7=rs.getString(7);
								}
							%>					
                            <b><%=s2%></b><br>
                            <b><%=s3%></b><br>
                            created by <%=s7%> at <%=s5%>

                            <%
                            	if (s6 != null) {
                            		out.print("<br>updated at "+s6+"<br><br><br>");
                            	} else {
                            		out.print("<br><br><br>");
	                            }
                            %>
                            <a href="update_post.jsp?post_id=<%=i%>" class="btn btn-success">Update</a>
                            <a href="delete_post.jsp?post_id=<%=i%>" class="btn btn-danger float-right">Delete</a><br>
						</div>
					</div>
				</div>
				<%
			}
			catch(Exception e)
			{
				out.println(e);
			}
		%>
	</div>
</body>
</html>
