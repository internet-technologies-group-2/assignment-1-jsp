<!DOCTYPE html>
<html>
<head>
	<title>INT Assignment</title>
</head>
<body>
	<div class="container">
	<%@ include file="navbar.jsp" %>
	<%@ include file="connect.jsp" %>
		<%
			String post_id=request.getParameter("post_id");
			String s2="",s3="",s4="",s5="",s6="",s7="";
			int i=0;
			try
			{
				String query="select * from posts where post_id = "+post_id; 
				Statement st=connection.createStatement();
				ResultSet rs=st.executeQuery(query);

				while ( rs.next() )
				{
					i=rs.getInt(1);
					s2=rs.getString(2);
					s3=rs.getString(3);
					s4=rs.getString(4);
					s5=rs.getString(5);
					s6=rs.getString(6);
					s7=rs.getString(7);
				}			
				%>
				<br>
				<div class="col-md-6"  style="width:1000px; margin:0 auto;">
					<div class="card">
						<div class="card-header  bg-info text-white text-center">
							Update Post
						</div>
						<div class="card-body">
							<form class="form-group" action="check_update_post.jsp" method="POST">
								<b>Title: </b><input type="text" name="title" required value="<%=s2%>" class="form-control col-md-12">
								<b>Description: </b>
								<textarea rows="4" cols="50"  name="description" required class="form-control col-md-12"><%=s3%></textarea>	
		                        <br>
		                        <input type="hidden" name="post_id" value="<%=i%>">
								<input type="submit" name="subBtn" value="Update" class="btn btn-warning">
								<a href="view_post.jsp?post_id=<%=i%>" class="btn btn-danger float-right">Cancel</a>
							</form>
						</div>
					</div>
				</div>
				<%
			}
			catch(Exception e)
			{
				out.println(e);
			}
		%>
	</div>
</body>
</html>
