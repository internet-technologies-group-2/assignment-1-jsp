<title>Authentication Page</title>
<%@ include file="connect.jsp" %>
<%@ page import="java.util.Date" %>
<%
	String title=request.getParameter("title");
	String description=request.getParameter("description");
	String name=(String)session.getAttribute("username");
	
    try {
		PreparedStatement ps=connection.prepareStatement("insert into posts(`post_title`, `post_description`, `post_created_by`) values(?,?,?)");
		ps.setString(1,title);
		ps.setString(2,description);
		ps.setString(3,name);

		int x=ps.executeUpdate();
		if(x>0) {
			out.print("Post Created Successfully");
			response.sendRedirect("posts.jsp");
		}
	} catch(Exception e) {
		out.print(e);
	}
%>